﻿using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Repository
{
    public interface IProductRepository
    {
        public int AddProduct(Product product);
        public int DeleteProduct(Product product);
        public int UpdateProduct(Product product);
        public List<Product> GetProduct();
        public Product? GetProductById(int id);
        Product? GetProductByName(string? name);
    }
}
