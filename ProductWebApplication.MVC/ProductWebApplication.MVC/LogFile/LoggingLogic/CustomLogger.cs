﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ProductWebApplication.MVC.LogFile.LoggingLogic
{
    public class CustomLogger : ActionFilterAttribute
    {
        readonly string logFileName;
        DateTime startTime;
        DateTime endTime;
        TimeSpan totalTime;
        public CustomLogger(IWebHostEnvironment environment)
        {
            logFileName = environment.ContentRootPath + @"/LogFile/CustomLogger.txt";
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            startTime = DateTime.Now;
            ControllerBase controllerBase = (ControllerBase)context.Controller;
            ControllerContext controllerContext = controllerBase.ControllerContext;
            String controllerName = controllerContext.ActionDescriptor.ControllerName;
            string actionName = controllerContext.ActionDescriptor.ActionName;
            using(StreamWriter writer = File.AppendText(logFileName))
            {
                writer.Write($"StartTime::{startTime}\tControllerName::{controllerName}\tActionName::{actionName}\t");
                writer.Close();
            }

        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
           endTime = DateTime.Now;
           totalTime = endTime - startTime;
           using (StreamWriter writer = File.AppendText(logFileName))
           {
                writer.Write($"EndTime::{endTime}\tTotalTime::{totalTime}");
                writer.WriteLine();
                writer.Close();
           }
        }
    }
}
