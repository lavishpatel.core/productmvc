﻿using Microsoft.EntityFrameworkCore;
using ProductWebApplication.MVC.Models;

namespace ProductWebApplication.MVC.Context
{
    public class ProductDbContext : DbContext
    {
        public ProductDbContext()
        {

        }
        public ProductDbContext(DbContextOptions<ProductDbContext> options) : base(options)
        {
            
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LoginUser>().HasNoKey();
        }
       
        public DbSet<ProductWebApplication.MVC.Models.LoginUser>? LoginUser { get; set; }
    }
}
