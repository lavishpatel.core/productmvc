﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductWebApplication.MVC.ExceptionAttribute;
using ProductWebApplication.MVC.LogFile.LoggingLogic;
using ProductWebApplication.MVC.Models;
using ProductWebApplication.MVC.Service;
using ProductWebApplication.MVC.Services;

namespace ProductWebApplication.MVC.Controllers
{
    [ExceptionHandler]
    [ServiceFilter(typeof(CustomLogger))]
    public class UserController : Controller
    {
        readonly IUserService _userService;
        readonly ITokenGenerator _tokenGenerator;
        readonly IConfiguration _configuration;
        public UserController(IUserService userService,ITokenGenerator tokenGenerator, IConfiguration configuration)
        {
            _userService = userService;
            _tokenGenerator = tokenGenerator;
            _configuration = configuration; 
        }
        [HttpGet]
        public ActionResult UserRegister()
        {
            ViewBag.Location = new List<string> { "Choose Your City", "Bangalore","Indore", "Bhopal", "Bombay" };
            return View();
        }

        [HttpPost]
        public ActionResult UserRegister(User user)
        {
            bool userAddStatus = _userService.AddUser(user);
            if (userAddStatus)
            {
                return RedirectToAction("GetAllUsers");
            }
            else
            {
                return Redirect("UserRegister");
            }
        }
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            List<User> users = _userService.GetAllUsers();
            return View(users);
        }
        [HttpGet]
        public ActionResult UserLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserLogin(LoginUser loginUser)
        {
            User user = _userService.Login(loginUser);
            string userToken = _tokenGenerator.GenerateToken(user.Id, user.Name);
            HttpContext.Session.SetString("USERTOKEN", userToken);
            return RedirectToAction("Dashboard");
        }
       // [Authorize]
        public ActionResult Dashboard()
        {
            string userToken = HttpContext.Session.GetString("USERTOKEN");
            Console.Write(userToken);
            if(userToken==null)
            {
                return RedirectToAction("UserLogin");
            }
            var userSecretKey = _configuration["JwtValidationDetails:UserApplicationSecretKey"];
            var userIssuer = _configuration["JwtValidationDetails:UserIssuer"];
            var userAudience = _configuration["JwtValidationDetails:UserAudience"];
            bool isValidToken = _tokenGenerator.IsValidToken(userSecretKey,userIssuer,userAudience,userToken);
            if(!isValidToken)
            {
                return RedirectToAction("UserLogin");
            }
             return View();
        }
    }
}
