﻿
using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.InMemory;
using ProductWebApplication.MVC.Context;
using ProductWebApplication.MVC.Models;

namespace MVCUserAppTest
{
    public class UserDbFixture
    {
        internal ProductDbContext _productDbContext;
        public UserDbFixture()
        {
            var userDbContexOptions = new DbContextOptionsBuilder<ProductDbContext>().UseInMemoryDatabase("UserDb").Options;
            _productDbContext = new ProductDbContext(userDbContexOptions);
            _productDbContext.Add(new User() { Id = 1, Name = "User1", Password = "User1", Location = "Bhopal" });
            _productDbContext.SaveChanges();
        }

    }
}