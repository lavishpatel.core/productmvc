﻿using ProductWebApplication.MVC.Models;
using ProductWebApplication.MVC.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MVCUserAppTest
{
    public class UserRepositoyTest : IClassFixture<UserDbFixture>
    {
        readonly UserRepository _userRepositoy;
        public UserRepositoyTest(UserDbFixture userDbFixture)
        {
            _userRepositoy = new UserRepository(userDbFixture._productDbContext);
        }
        [Fact]
        public void AddUserReturnSuccess()
        {
            //Intialization
            string ExpectedUserName = "User2";
            //Call Devlopment Code
            _userRepositoy.AddUser(new User() { Id = 2, Name = "User2", Password = "User2", Location = "Indore" });
            var ActualUserName = _userRepositoy.GetAllUsers()[1].Name;
            //Assert
            Assert.Equal(ExpectedUserName, ActualUserName);
        }
    }
}
