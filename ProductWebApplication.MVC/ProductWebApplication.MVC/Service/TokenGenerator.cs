﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ProductWebApplication.MVC.Service
{
    public class TokenGenerator : ITokenGenerator
    {
        public string GenerateToken(int id, string name)
        {
            var userClaims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Jti,new Guid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName,name)

            };
            var userSecurityKey = Encoding.UTF8.GetBytes("kjhfdertgfhgfdsawertyuiokjhgnbvb");
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var userSigninCredentials = new SigningCredentials(userSymmetricSecurity,SecurityAlgorithms.HmacSha256);
            var userJwtSecurityToken = new JwtSecurityToken
                (
                    issuer: "ProductApp",
                    audience: "coreMVCuser",
                    claims: userClaims,
                    expires: DateTime.UtcNow.AddMinutes(10),
                    signingCredentials: userSigninCredentials
                ) ;
            var userSecurityTokenHandler = new JwtSecurityTokenHandler().WriteToken(userJwtSecurityToken);
            return userSecurityTokenHandler;
        }

        public bool IsValidToken(string userSecretKey, string userIssuer, string userAudience, string userToken)
        {
            var userSecurityKey = Encoding.UTF8.GetBytes(userSecretKey);
            var userSymmetricSecurity = new SymmetricSecurityKey(userSecurityKey);
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = userIssuer,

                ValidateAudience = true,
                ValidAudience = userAudience,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = userSymmetricSecurity,

                ValidateLifetime = true
            };
            JwtSecurityTokenHandler tokenValidationHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenValidationHandler.ValidateToken(userToken, tokenValidationParameters, out SecurityToken securityToken);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
